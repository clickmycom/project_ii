


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Window;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.text.JTextComponent;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.Font;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JWindow;
import javax.swing.KeyStroke;
import javax.swing.UIManager;





























import com.mysql.jdbc.ConnectionImpl;

import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;



public class specify extends JInternalFrame {

	private JPanel contentPane;
	private JLabel lblFile;
	private Connection conn = new Connect().getConnection();
	private JComboBox comboBox_Bureau;
	private JComboBox comboBox_Program;
	private JComboBox combo_search_name_tec;
	//รับค่า เพื่อให้รู้ว่าผู้ใช้เลือกอะไร
	private ArrayList arrCarTypeId = new ArrayList(); 
	private ArrayList arrCodeId = new ArrayList(); 
	private ArrayList arrBureauId = new ArrayList(); 
	private JTextComponent txt_Address_1;
	private ArrayList arr_search_name_tec = new ArrayList(); 
	String tana="";
	private JPanel panel;
	File file;
	Image img;
	 picpanel p;
	JPanel panel_pic;
	GridBagConstraints gbc=new GridBagConstraints();
	private  ObservingTextField txt_date;
	private JTextField txt_room;
	private JTextField textField_1;

	private JComboBox combo_date;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Locale.setDefault(Locale.ENGLISH);
			String name = "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel";
			UIManager.setLookAndFeel(name);
			
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Register_TEC frame = new Register_TEC();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public specify() {
		setClosable(true);
		setIconifiable(true);
		
		
		
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameOpened(InternalFrameEvent e) {
				
				startForm();
			}
		
			
			private void startForm() {
				
				
		        
				try {
				
					
					String sql_rs_bureau = "SELECT * FROM bureau";
					ResultSet rs_bureau = conn.createStatement().executeQuery(sql_rs_bureau);
					
					String sql_rs_bureau_TEC = "SELECT * FROM bureau";
					ResultSet rs_bureau_TEC = conn.createStatement().executeQuery(sql_rs_bureau_TEC);
					
					
					combo_date.addItem("วันจันทร์");
					combo_date.addItem("วันอังคาร");
					combo_date.addItem("วันพุธ");
					combo_date.addItem("วันพฤหัสบดี");
					combo_date.addItem("วันศุกร์");
					combo_date.addItem("วันเสาร์");
					combo_date.addItem("วันอาทิตย์");
					
					Object cmboitem = combo_date.getSelectedItem();
					
					
					while (rs_bureau.next()) {
						
						comboBox_Bureau.addItem(rs_bureau.getString("Bureau_Name"));	
						arrBureauId.add(rs_bureau.getInt("Code_Bureau"));
					}
					
					
						
						
					
				
					
					// กำหนดค่าเช่าเริ่มต้น คิดจากค่าใน dropdownList
				
					int index1 = comboBox_Bureau.getSelectedIndex();
					int index2 = comboBox_Program.getSelectedIndex();
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		setTitle("กำหนดผู้สอน");
		setBounds(100, 100, 776, 588);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 11, 742, 474);
		contentPane.add(tabbedPane);
		
		panel = new JPanel();
		tabbedPane.addTab("กำหนดอาจารย์ผู้สอน และ เวลาที่สอน", null, panel, null);
		panel.setLayout(null);
		
		JLabel Fname = new JLabel("ชื่ออาจารย์");
		Fname.setBounds(26, 123, 78, 14);
		panel.add(Fname);
		Fname.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		
		comboBox_Bureau = new JComboBox();
		comboBox_Bureau.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				comboBox_Program.removeAllItems();  // เมื่อมีการเปลี่ยน ให้ทำการลบ ข้อมูลที่มีใน combobox ออกก่อน
				
				//หลักการคือ  เก็บindexที่ผู้ใช้เลือก -> ค้นหารหัส สาขาวิชา -> ถ้าเจอข้อมูล -> ทำการค้นหาตามรหัส สาขาวิชา ที่เลือกเข้ามา -> เพิ่มข้อมูลไป combobox
				try {
					int index31 = comboBox_Bureau.getSelectedIndex();
				
					index31+=1;  // ต้อง +1 ให้มันเพราะมัน getindex ตอน select มาได้ 0 แต่ในฐานข้อมูลเป็น 1 2 3 
					String strI = Integer.toString(index31);
					
					//ค้นหาข้อมูล index ที่ผู้ใช้ทำการเลือก 
					String sql_1 = "SELECT `Code_Bureau` FROM `bureau` WHERE `Code_Bureau`=?";
					PreparedStatement rs_bureau1 = conn.prepareStatement(sql_1);
					
					rs_bureau1.setString(1, strI);
					ResultSet rr1 = rs_bureau1.executeQuery();
					
					//ถ้าเจอข้อมูล
					if(rr1.next()){
						tana=rr1.getString("Code_Bureau");
						
						//ทำการค้นหา รหัสสาขาวิชา ที่ผู้ใช้เลือก
						String sql_program = "SELECT * FROM `tb_program` WHERE `Bureau_Code_Bureau`="+rr1.getString("Code_Bureau");
						ResultSet rs_program = conn.createStatement().executeQuery(sql_program);
						
						//วนลูปทำการเพิ่มข้อมูลลงไปใน combobox
						while (rs_program.next()) {
							
								comboBox_Program.addItem(rs_program.getString("PROGRAM_NAME"));	
								arrCodeId.add(rs_program.getInt("Index_Group_Program"));	
						}				
					}

					
				} catch (SQLException e1) {
					
					e1.printStackTrace();
				}
				
				
			}
		});
		comboBox_Bureau.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		comboBox_Bureau.setBounds(189, 11, 542, 39);
		panel.add(comboBox_Bureau);
		
		comboBox_Program = new JComboBox();
		comboBox_Program.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				
				
				combo_search_name_tec.removeAllItems();  // เมื่อมีการเปลี่ยน ให้ทำการลบ ข้อมูลที่มีใน combobox ออกก่อน
				
				//หลักการคือ  เก็บindexที่ผู้ใช้เลือก -> ค้นหารหัส สาขาวิชา -> ถ้าเจอข้อมูล -> ทำการค้นหาตามรหัส สาขาวิชา ที่เลือกเข้ามา -> เพิ่มข้อมูลไป combobox
				try {
					int index37511_1 = comboBox_Bureau.getSelectedIndex();
					int index37511_2 = comboBox_Program.getSelectedIndex();
					index37511_1+=1;
					index37511_2+=1;
					
					String sql_in36511="SELECT `PROGRAM_NAME` FROM `tb_program` WHERE `Index_Group_Program`=? AND `Bureau_Code_Bureau` =?";
					PreparedStatement rs_bureau132 = conn.prepareStatement(sql_in36511);
					
					String strI_A = Integer.toString(index37511_2);
					String strI_B = Integer.toString(index37511_1);
					
				
					rs_bureau132.setString(1, strI_A);
					rs_bureau132.setString(2, strI_B);
					
					ResultSet rr1_AABB = rs_bureau132.executeQuery(); //สิ่งที่ได้คือ ชื่อสาขา
					

					//ถ้าเจอข้อมูล
					if(rr1_AABB.next()){
					
						
						//ค้นหาข้อมูล สาขาวิชา  ที่ผู้ใช้ทำการเลือก 
						String sql_1PP = "SELECT * FROM `bibliography_techer` WHERE `CODE_PROGRAM` =?";
						PreparedStatement rs_search_TECHERE = conn.prepareStatement(sql_1PP);
						

						rs_search_TECHERE.setString(1,rr1_AABB.getString("PROGRAM_NAME"));
						ResultSet rs_search_TECHERE_YES = rs_search_TECHERE.executeQuery();
						

							//วนลูปทำการเพิ่มข้อมูลลงไปใน combobox
							while (rs_search_TECHERE_YES.next()) {
								
								combo_search_name_tec.addItem(rs_search_TECHERE_YES.getString("FIRSTNAME")+"  "+rs_search_TECHERE_YES.getString("FNAME")+"   "+rs_search_TECHERE_YES.getString("LNAME"));	
								arr_search_name_tec.add(rs_search_TECHERE_YES.getInt("NO_TEC"));	
							}
						
							
						
							
					
							
					}
						
					  
					
				} catch (SQLException e1) {
					
					e1.printStackTrace();
				}
		
				
				
			}
		});
		comboBox_Program.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		comboBox_Program.setBounds(189, 61, 542, 39);
		panel.add(comboBox_Program);
		
		JLabel label_6 = new JLabel("\u0E2A\u0E33\u0E19\u0E31\u0E01\u0E27\u0E34\u0E0A\u0E32");
		label_6.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		label_6.setBounds(26, 16, 117, 29);
		panel.add(label_6);
		
		JLabel label_7 = new JLabel("\u0E2A\u0E32\u0E02\u0E32\u0E27\u0E34\u0E0A\u0E32");
		label_7.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		label_7.setBounds(26, 66, 117, 29);
		panel.add(label_7);
		
		combo_search_name_tec = new JComboBox();
		combo_search_name_tec.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		combo_search_name_tec.setBounds(189, 111, 542, 39);
		panel.add(combo_search_name_tec);
		
		final JComboBox combo_hour_in = new JComboBox();
		
		combo_hour_in.addItem(" 8.00  น.");
		combo_hour_in.addItem(" 9.00  น.");
		combo_hour_in.addItem("10.00  น.");
		combo_hour_in.addItem("11.00  น.");
		combo_hour_in.addItem("12.00  น.");
		combo_hour_in.addItem("13.00  น.");
		combo_hour_in.addItem("14.00  น.");
		combo_hour_in.addItem("15.00  น.");
		combo_hour_in.addItem("16.00  น.");
		combo_hour_in.addItem("17.00  น.");
		combo_hour_in.addItem("18.00  น.");
		combo_hour_in.addItem("19.00  น.");
		combo_hour_in.addItem("20.00  น.");
		
		combo_hour_in.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		combo_hour_in.setBounds(189, 270, 289, 39);
		panel.add(combo_hour_in);
		
		final JComboBox combo_hour_out = new JComboBox();
		combo_hour_out.addItem(" 8.00  น.");
		combo_hour_out.addItem(" 9.00  น.");
		combo_hour_out.addItem("10.00  น.");
		combo_hour_out.addItem("11.00  น.");
		combo_hour_out.addItem("12.00  น.");
		combo_hour_out.addItem("13.00  น.");
		combo_hour_out.addItem("14.00  น.");
		combo_hour_out.addItem("15.00  น.");
		combo_hour_out.addItem("16.00  น.");
		combo_hour_out.addItem("17.00  น.");
		combo_hour_out.addItem("18.00  น.");
		combo_hour_out.addItem("19.00  น.");
		combo_hour_out.addItem("20.00  น.");
		combo_hour_out.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		combo_hour_out.setBounds(189, 322, 289, 39);
		panel.add(combo_hour_out);
		
		JLabel label = new JLabel("เริ่มชั่วโมงสอน");
		label.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		label.setBounds(26, 282, 78, 14);
		panel.add(label);
		
		JLabel label_1 = new JLabel("หมดชั่วโมงสอน");
		label_1.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		label_1.setBounds(26, 334, 92, 14);
		panel.add(label_1);
		
		JLabel label_2 = new JLabel("ห้องสอน");
		label_2.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		label_2.setBounds(26, 382, 92, 14);
		panel.add(label_2);
		
		txt_room = new JTextField();
		txt_room.setBounds(189, 372, 289, 39);
		panel.add(txt_room);
		txt_room.setColumns(10);
		
		JLabel label_4 = new JLabel("วิชาที่สอน (ระบุเป็นรหัสวิชา)");
		label_4.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		label_4.setBounds(26, 175, 162, 14);
		panel.add(label_4);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(189, 165, 289, 39);
		panel.add(textField_1);
		
		JLabel date_box = new JLabel("วันสอน");
		date_box.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		date_box.setBounds(26, 230, 92, 14);
		panel.add(date_box);
		
		combo_date = new JComboBox();
		combo_date.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		combo_date.setBounds(189, 220, 289, 39);
		panel.add(combo_date);
	
		
		
		//ส่วนของการบันทึกข้อมูลลง Database
		JButton btn_save = new JButton("\u0E1A\u0E31\u0E19\u0E17\u0E36\u0E01\u0E02\u0E49\u0E2D\u0E21\u0E39\u0E25");
		btn_save.setIcon(new ImageIcon("C:\\Users\\clickmycom\\workspace\\Project_II\\assets\\filesave.png"));
		btn_save.setBounds(31, 496, 719, 43);
		contentPane.add(btn_save);
		btn_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					

					String sql = ""
							+ " INSERT INTO specify("
							
							+ "FK_TEACHER_CODE,"
							+ "date,"
							+ "in_hours,"
							+ "out_hours,"
							+ "room,"
							+ "FK_code_subject"
							+ " ) VALUES(?,?,?,?,?,?)";
					
		
					
					int index1 = comboBox_Bureau.getSelectedIndex();
					int index2 = comboBox_Program.getSelectedIndex();
					int index_combo_date = combo_date.getSelectedIndex();
					int index_combo_hour_in = combo_hour_in.getSelectedIndex();
					int index_combo_hour_out = combo_hour_out.getSelectedIndex();
					
					
					
					PreparedStatement pre = conn.prepareStatement(sql);
					
					int index_name_TEC=combo_search_name_tec.getSelectedIndex();
					index_name_TEC+=1;
					
					//หาชื่อครู
					String sql_Z1 = "SELECT `TEACHER_CODE` FROM `bibliography_techer` WHERE `NO_TEC` =?";
					PreparedStatement rs_bureau1AZ;
					rs_bureau1AZ = conn.prepareStatement(sql_Z1);
					
					rs_bureau1AZ.setInt(1,index_name_TEC);
					ResultSet rr1AZ = rs_bureau1AZ.executeQuery();
					
					
					if(rr1AZ.next()){

						pre.setString(1,rr1AZ.getString("TEACHER_CODE")); 
						
					}
					// จบ  หาชื่อครู
					
					
					//เริ่ม  หาวัน
					
					switch(index_combo_date){
						case 0 :
							pre.setString(2,"วันจันทร์");
			        	break;
						case 1 :
							pre.setString(2,"วันอังคาร");
			        	break;
						case 2 :
							pre.setString(2,"วันพุธ");
			        	break;
						case 3 :
							pre.setString(2,"วันพฤหัสบดี");
			        	break;
						case 4 :
							pre.setString(2,"วันศุกร์");
			        	break;
						case 5 :
							pre.setString(2,"วันเสาร์");
			        	break;
						case 6 :
							pre.setString(2,"วันอาทิตย์");
			        	break;

					}
					
					//จบ หาวัน
					
				switch(index_combo_hour_in){
					case 0 :
							pre.setString(3,"8.00");
					break;
					case 1 :
							pre.setString(3,"9.00");
					break;
					case 2 :
							pre.setString(3,"10.00");
					break;
					case 3 :
							pre.setString(3,"11.00");
					break;
					case 4 :
							pre.setString(3,"12.00");
					break;
					case 5 :
							pre.setString(3,"13.00");
					break;
					case 6 :
							pre.setString(3,"14.00");
					break;
					case 7 :
							pre.setString(3,"15.00");
					break;
					case 8 :
							pre.setString(3,"16.00");
					break;
					case 9 :
							pre.setString(3,"17.00");
					break;
					case 10 :
							pre.setString(3,"18.00");
					break;
					case 11 :
							pre.setString(3,"19.00");
					break;
					case 12 :
							pre.setString(3,"20.00");
					break;
				}
				
				
				switch(index_combo_hour_out){
				case 0 :
						pre.setString(4,"8.00");
				break;
				case 1 :
						pre.setString(4,"9.00");
				break;
				case 2 :
						pre.setString(4,"10.00");
				break;
				case 3 :
						pre.setString(4,"11.00");
				break;
				case 4 :
						pre.setString(4,"12.00");
				break;
				case 5 :
						pre.setString(4,"13.00");
				break;
				case 6 :
						pre.setString(4,"14.00");
				break;
				case 7 :
						pre.setString(4,"15.00");
				break;
				case 8 :
						pre.setString(4,"16.00");
				break;
				case 9 :
						pre.setString(4,"17.00");
				break;
				case 10 :
						pre.setString(4,"18.00");
				break;
				case 11 :
						pre.setString(4,"19.00");
				break;
				case 12 :
						pre.setString(4,"20.00");
				break;
			}
					
				pre.setString(5,txt_room.getText());
				
				pre.setString(6,textField_1.getText());
				
					//บันทึกข้อมูล
					pre.executeUpdate();
	
					int type = JOptionPane.INFORMATION_MESSAGE;
					String str = "ข้อมูลเรียบร้อยแล้ว";
					JOptionPane.showMessageDialog(null, str, "ข้อมูลเรียบร้อยแล้ว", type);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				
			}
		});
		btn_save.setFont(new Font("AngsanaUPC", Font.BOLD, 18));
		//จบ ส่วนของการบันทึกข้อมูลลง Database
	
		
		
		
}
}


