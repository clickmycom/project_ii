

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.text.JTextComponent;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.Font;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import com.mysql.jdbc.ConnectionImpl;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;


public class Register_STD extends JInternalFrame {

	private JPanel contentPane;
	private JTextField txt_Fname;
	private JLabel lblFile;
	private Connection conn = new Connect().getConnection();
	
	private JComboBox ddlFristName;
	private JComboBox comboBox_Bureau;
	private JComboBox comboBox_Program;
	private JComboBox combo_status_std;
	
	private JComboBox combo_search_bu_TEC;
	private JComboBox combo_seart_code_TEH;
	private JComboBox combo_search_name_tec;
	
	//รับค่า เพื่อให้รู้ว่าผู้ใช้เลือกอะไร
	private ArrayList arrCarTypeId = new ArrayList(); 
	private ArrayList arrCodeId = new ArrayList(); 
	private ArrayList arrBureauId = new ArrayList(); 
	private ArrayList arrState = new ArrayList(); 
	private ArrayList arr_search_bu_TEC = new ArrayList(); 
	private ArrayList arr_seart_code_TEH = new ArrayList(); 
	private ArrayList arr_search_name_tec = new ArrayList(); 
	
	private JTextField txt_Lname;
	private JTextField txt_code;
	private JTextComponent txt_Address_1;
	String tana="";
	
	String Img_TEC="";
	
	File file;
	Image img;
	Image img1;
	 picpanel p;
	 picpanel p2;
	JPanel panel_pic;
	JPanel panel_pic_TEC;
	GridBagConstraints gbc=new GridBagConstraints();
	GridBagConstraints gbc2=new GridBagConstraints();
	private  ObservingTextField txt_date;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Locale.setDefault(Locale.ENGLISH);
			String name = "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel";
			UIManager.setLookAndFeel(name);
			
			//Register_TEC frame = new Register_TEC();
			//frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Register_STD frame = new Register_STD();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Register_STD() {
		setClosable(true);
		setIconifiable(true);
		
		
		
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameOpened(InternalFrameEvent e) {
				
				startForm();
			}
		
			
			private void startForm() {
				try {
					
					String sql = "SELECT * FROM tb_fname_type";
					ResultSet rs = conn.createStatement().executeQuery(sql);
					
					String sql_rs_bureau = "SELECT * FROM bureau";
					ResultSet rs_bureau = conn.createStatement().executeQuery(sql_rs_bureau);
					
					String sql_rs_state = "SELECT * FROM tb_state";
					ResultSet rs_state = conn.createStatement().executeQuery(sql_rs_state);
					
					
					String sql_rs_bureau_TEC = "SELECT * FROM bureau";
					ResultSet rs_bureau_TEC = conn.createStatement().executeQuery(sql_rs_bureau_TEC);
					
					while (rs.next()) {
						ddlFristName.addItem(rs.getString("type_frist_name"));	
						arrCarTypeId.add(rs.getInt("idtb_fname_type"));
					}
					
					while (rs_bureau.next()) {
						
						comboBox_Bureau.addItem(rs_bureau.getString("Bureau_Name"));	
						arrBureauId.add(rs_bureau.getInt("Code_Bureau"));
					}
					
					while (rs_state.next()) {
						
						combo_status_std.addItem(rs_state.getString("state_name"));	
						arrState.add(rs_state.getInt("NO"));
					}

					while (rs_bureau_TEC.next()) {
						
						combo_search_bu_TEC.addItem(rs_bureau_TEC.getString("Bureau_Name"));	
						arr_search_bu_TEC.add(rs_bureau_TEC.getInt("Code_Bureau"));
					}

					
					// กำหนดค่าเช่าเริ่มต้น คิดจากค่าใน dropdownList
					int index = ddlFristName.getSelectedIndex();
					int index1 = comboBox_Bureau.getSelectedIndex();
					int index2 = comboBox_Program.getSelectedIndex();
					int index_state=combo_search_bu_TEC.getSelectedIndex();
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		setTitle("ลงทะเบียนนักศึกษา");
		setBounds(100, 100, 776, 588);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 11, 742, 474);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("ข้อมูลส่วนตัว", null, panel, null);
		panel.setLayout(null);
		
		final JTextArea txt_Address_1_1 = new JTextArea();
		txt_Address_1_1.setBounds(153, 336, 578, 94);
		panel.add(txt_Address_1_1);
		txt_Address_1_1.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		
		JLabel label_3 = new JLabel("\u0E17\u0E35\u0E48\u0E2D\u0E22\u0E39\u0E48");
		label_3.setBounds(26, 372, 117, 29);
		panel.add(label_3);
		label_3.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		
		JLabel txt_input_lern = new JLabel("สถานภาพ");
		txt_input_lern.setBounds(26, 295, 117, 20);
		panel.add(txt_input_lern);
		txt_input_lern.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		
		txt_code = new JTextField();
		txt_code.setBounds(153, 227, 578, 45);
		panel.add(txt_code);
		txt_code.setFont(new Font("Angsana New", Font.PLAIN, 16));
		txt_code.setColumns(10);
		
		JLabel label_1 = new JLabel("\u0E2B\u0E21\u0E32\u0E22\u0E40\u0E25\u0E02\u0E1A\u0E31\u0E15\u0E23\u0E1B\u0E23\u0E30\u0E0A\u0E32\u0E0A\u0E19");
		label_1.setBounds(26, 242, 117, 14);
		panel.add(label_1);
		label_1.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		
		txt_Lname = new JTextField();
		txt_Lname.setBounds(492, 171, 239, 45);
		panel.add(txt_Lname);
		txt_Lname.setFont(new Font("Angsana New", Font.PLAIN, 16));
		txt_Lname.setColumns(10);
		
		JLabel Lname = new JLabel("\u0E19\u0E32\u0E21\u0E2A\u0E01\u0E38\u0E25");
		Lname.setBounds(421, 186, 46, 14);
		panel.add(Lname);
		Lname.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		
		txt_Fname = new JTextField();
		txt_Fname.setBounds(153, 171, 245, 45);
		panel.add(txt_Fname);
		txt_Fname.setFont(new Font("Angsana New", Font.PLAIN, 16));
		txt_Fname.setColumns(10);
		
		JLabel Fname = new JLabel("\u0E0A\u0E37\u0E48\u0E2D");
		Fname.setBounds(26, 186, 46, 14);
		panel.add(Fname);
		Fname.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		
		ddlFristName = new JComboBox();
		ddlFristName.setBounds(153, 118, 245, 39);
		panel.add(ddlFristName);
		ddlFristName.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		
		JLabel label = new JLabel("\u0E04\u0E33\u0E02\u0E36\u0E49\u0E19\u0E15\u0E49\u0E19");
		label.setBounds(26, 130, 46, 14);
		panel.add(label);
		label.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		
		comboBox_Bureau = new JComboBox();
		comboBox_Bureau.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				
				comboBox_Program.removeAllItems();  // เมื่อมีการเปลี่ยน ให้ทำการลบ ข้อมูลที่มีใน combobox ออกก่อน
				
				//หลักการคือ  เก็บindexที่ผู้ใช้เลือก -> ค้นหารหัส สาขาวิชา -> ถ้าเจอข้อมูล -> ทำการค้นหาตามรหัส สาขาวิชา ที่เลือกเข้ามา -> เพิ่มข้อมูลไป combobox
				try {
					int index31 = comboBox_Bureau.getSelectedIndex();
				
					index31+=1;  // ต้อง +1 ให้มันเพราะมัน getindex ตอน select มาได้ 0 แต่ในฐานข้อมูลเป็น 1 2 3 
					String strI = Integer.toString(index31);
					
					//ค้นหาข้อมูล index ที่ผู้ใช้ทำการเลือก 
					String sql_1 = "SELECT `Code_Bureau` FROM `bureau` WHERE `Code_Bureau`=?";
					PreparedStatement rs_bureau1 = conn.prepareStatement(sql_1);
					
					rs_bureau1.setString(1, strI);
					ResultSet rr1 = rs_bureau1.executeQuery();
					
					//ถ้าเจอข้อมูล
					if(rr1.next()){
						tana=rr1.getString("Code_Bureau");
						
						//ทำการค้นหา รหัสสาขาวิชา ที่ผู้ใช้เลือก
						String sql_program = "SELECT * FROM `tb_program` WHERE `Bureau_Code_Bureau`="+rr1.getString("Code_Bureau");
						ResultSet rs_program = conn.createStatement().executeQuery(sql_program);
						
						//วนลูปทำการเพิ่มข้อมูลลงไปใน combobox
						while (rs_program.next()) {
							
								comboBox_Program.addItem(rs_program.getString("PROGRAM_NAME"));	
								arrCodeId.add(rs_program.getInt("Index_Group_Program"));	
						}				
					}

					
				} catch (SQLException e1) {
					
					e1.printStackTrace();
				}
				
				
			}
		});
		comboBox_Bureau.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		comboBox_Bureau.setBounds(153, 11, 578, 39);
		panel.add(comboBox_Bureau);
		
		comboBox_Program = new JComboBox();
		comboBox_Program.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		comboBox_Program.setBounds(153, 61, 578, 39);
		panel.add(comboBox_Program);
		
		JLabel label_6 = new JLabel("\u0E2A\u0E33\u0E19\u0E31\u0E01\u0E27\u0E34\u0E0A\u0E32");
		label_6.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		label_6.setBounds(26, 16, 117, 29);
		panel.add(label_6);
		
		JLabel label_7 = new JLabel("\u0E2A\u0E32\u0E02\u0E32\u0E27\u0E34\u0E0A\u0E32");
		label_7.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		label_7.setBounds(26, 66, 117, 29);
		panel.add(label_7);
		
		combo_status_std = new JComboBox();
		combo_status_std.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		combo_status_std.setBounds(153, 286, 245, 39);
		panel.add(combo_status_std);
		
		final JPanel panel_1 = new JPanel();
		tabbedPane.addTab("รูปภาพ", null, panel_1, null);
		panel_1.setLayout(null);

		/////////////////////////////////////////////////////////////////////
		//เริ่มมม   การสร้างกรอบ Panel_2 ส่วนดึงรูปอาจารย์
		final JPanel panel_2 = new JPanel();
		tabbedPane.addTab("อาจารย์ที่ปรึกษา", null, panel_2, null);
		panel_2.setLayout(null);
		

		//ส่วนของการโหลดภาพลงมาแล้วใส่รูปในกรอบ
		 panel_pic_TEC = new JPanel();
		panel_pic_TEC.setBounds(229, 11, 299, 253);
		panel_2.add(panel_pic_TEC);
		panel_pic_TEC.setLayout(new BorderLayout());
		
		//สร้าง กรอบของ Panel
		  gbc2.gridx=2;
		  gbc2.gridy=0;
		  gbc2.gridheight=3;
		  gbc2.gridwidth=3;
		  gbc2.insets=new Insets(10,60,20,40);
		  Border compound2,raisedbevel2, loweredbevel2;
		  raisedbevel2 = BorderFactory.createRaisedBevelBorder();
		  loweredbevel2 = BorderFactory.createLoweredBevelBorder();
		  compound2 = BorderFactory.createCompoundBorder(
					  raisedbevel2, loweredbevel2);
		  panel_pic_TEC.setBorder(compound2);
		  panel_pic_TEC.setPreferredSize(new Dimension(100,140));
		  panel_2.add(panel_pic_TEC,gbc2);  //เพิ่ม Object เข้าไปใน panel ที่ 2
		  
		  
		  class ImageImplement extends JPanel { private Image imgxxxx; public ImageImplement(Image imgxxxx) { this.imgxxxx = imgxxxx; Dimension size = new Dimension(imgxxxx.getWidth(null), imgxxxx.getHeight(null)); setPreferredSize(size); setMinimumSize(size); setMaximumSize(size); setSize(size); setLayout(null); } public void paintComponent(Graphics g) { g.drawImage(imgxxxx, 0, 0, null); } }

		
		  
		   
		  
		  
		  
		  
		  
		  
		 // จบการสร้างกรอบ Panel_2 ส่วนดึงรูปอาจารย์
		////////////////////////////////////////////////////////////////////////////  
		
		final JLabel lblFiles = new JLabel("ชื่อไฟล์");
		lblFiles.setBounds(260, 305, 289, 14);
		panel_1.add(lblFiles);
		
		
		//ส่วนของการโหลดภาพลงมาแล้วใส่รูปในกรอบ
		final JPanel panel_pic = new JPanel();
		panel_pic.setBounds(250, 27, 299, 253);
		panel_1.add(panel_pic);
		panel_pic.setLayout(new BorderLayout());
		
		//สร้าง กรอบของ Panel
		  gbc.gridx=2;
		  gbc.gridy=0;
		  gbc.gridheight=3;
		  gbc.gridwidth=3;
		  gbc.insets=new Insets(10,60,20,40);
		  Border compound,raisedbevel, loweredbevel;
		  raisedbevel = BorderFactory.createRaisedBevelBorder();
		  loweredbevel = BorderFactory.createLoweredBevelBorder();
		  compound = BorderFactory.createCompoundBorder(
					  raisedbevel, loweredbevel);
		  panel_pic.setBorder(compound);
		  panel_pic.setPreferredSize(new Dimension(100,140));
		  panel_1.add(panel_pic,gbc);  //เพิ่ม Object เข้าไปใน panel ที่ 2
		  
		 // จบการสร้างกรอบ Panel
		  
		JButton btn_select_pic = new JButton("\u0E40\u0E25\u0E37\u0E2D\u0E01\u0E23\u0E39\u0E1B");
		btn_select_pic.setIcon(new ImageIcon("C:\\Users\\clickmycom\\workspace\\Project_II\\assets\\folder_open.png"));
		btn_select_pic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				 
				JFileChooser fileopen = new JFileChooser();
                int ret = fileopen.showDialog(null, "เลือกไฟล์นี้");
                
                
                fileopen.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                
                fileopen.setCurrentDirectory(new File("."));
               
                
              
                
                //ถ้าเลือกไฟล์เลือกแล้ว
                if (ret == JFileChooser.APPROVE_OPTION) {
                	  file = fileopen.getSelectedFile();
					  img = getToolkit().getImage(file.getPath());
					  p=new picpanel(img); 
					  lblFiles.setText(fileopen.getSelectedFile().toString());
					 
                }
                
                else{ //กรณีไม่เลือกไฟล์
                	JOptionPane.showMessageDialog(null,"คุณยังไม่ได้เลือกไฟล์ใดๆ");
                	
                }
                
                	panel_pic.add(p);  //นำภาพใส่ในกรอบที่สร้างขึ้น
                	
                	panel_pic.doLayout(); //สั่งให้มันเกิด event ในกรอบอีกทีนึง เนื่องจากปัญหาเลือกภาพแล้วมันไม่ยอมแสดงภาพในกรอบให้
                	
			}
		});
		
		class picpanel extends JPanel
		{
		 Image imgp;
		 picpanel(Image img)
		 {
		  this.imgp = img;
		  repaint();
		 }
		 
		// Fires whenever this panel is resized
		 public void paintComponent(Graphics g) 
		 {
		  super.paintComponent(g);
		  g.drawImage(imgp, 0, 0, getWidth(), getHeight(),this);
		 }
		}

			
		btn_select_pic.setBounds(260, 331, 289, 40);
		panel_1.add(btn_select_pic);
		
		// ส่วนของ วันที่
		txt_date = new ObservingTextField();
		txt_date.setBounds(260, 390, 289, 28);
		panel_1.add(txt_date);
		txt_date.setColumns(10);
		
		JButton btnDate = new JButton("วันที่");
		btnDate.setIcon(new ImageIcon("C:\\Users\\clickmycom\\workspace\\Project_II\\assets\\calendar.png"));
		btnDate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String lang = null;
				final Locale locale = getLocale(lang);
				DatePicker dp = new DatePicker(txt_date,locale);
				
				Date selectedDate = dp.parseDate(txt_date.getText());
				dp.setSelectedDate(selectedDate);
				dp.start(txt_date);
			}
			
		private Locale getLocale(String loc){
			if(loc != null && loc.length()>0)
				return new Locale(loc);
			else
				return Locale.US;
		}
		});
		btnDate.setBounds(562, 393, 89, 23);
		panel_1.add(btnDate);
		
		JLabel label_4 = new JLabel("ปีการศึกษาที่เข้า");
		label_4.setBounds(147, 397, 103, 14);
		panel_1.add(label_4);
		
		
		
		
		// เริ่ม ส่วนของ  คลิกเลือกสาขาวิชา แล้วชื่ออาจารย์เปลี่ยน
		combo_seart_code_TEH = new JComboBox();
		combo_seart_code_TEH.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				
				
				combo_search_name_tec.removeAllItems();  // เมื่อมีการเปลี่ยน ให้ทำการลบ ข้อมูลที่มีใน combobox ออกก่อน
				
				//หลักการคือ  เก็บindexที่ผู้ใช้เลือก -> ค้นหารหัส สาขาวิชา -> ถ้าเจอข้อมูล -> ทำการค้นหาตามรหัส สาขาวิชา ที่เลือกเข้ามา -> เพิ่มข้อมูลไป combobox
				try {
					int index37511_1 = combo_search_bu_TEC.getSelectedIndex();
					int index37511_2 = combo_seart_code_TEH.getSelectedIndex();
					index37511_1+=1;
					index37511_2+=1;
					
					String sql_in36511="SELECT `PROGRAM_NAME` FROM `tb_program` WHERE `Index_Group_Program`=? AND `Bureau_Code_Bureau` =?";
					PreparedStatement rs_bureau132 = conn.prepareStatement(sql_in36511);
					
					String strI_A = Integer.toString(index37511_2);
					String strI_B = Integer.toString(index37511_1);
					
				
					rs_bureau132.setString(1, strI_A);
					rs_bureau132.setString(2, strI_B);
					
					ResultSet rr1_AABB = rs_bureau132.executeQuery(); //สิ่งที่ได้คือ ชื่อสาขา
					

					//ถ้าเจอข้อมูล
					if(rr1_AABB.next()){
					
						
						//ค้นหาข้อมูล สาขาวิชา  ที่ผู้ใช้ทำการเลือก 
						String sql_1PP = "SELECT * FROM `bibliography_techer` WHERE `CODE_PROGRAM` =?";
						PreparedStatement rs_search_TECHERE = conn.prepareStatement(sql_1PP);
						

						rs_search_TECHERE.setString(1,rr1_AABB.getString("PROGRAM_NAME"));
						ResultSet rs_search_TECHERE_YES = rs_search_TECHERE.executeQuery();
						

							//วนลูปทำการเพิ่มข้อมูลลงไปใน combobox
							while (rs_search_TECHERE_YES.next()) {
								
								combo_search_name_tec.addItem(rs_search_TECHERE_YES.getString("FIRSTNAME")+"  "+rs_search_TECHERE_YES.getString("FNAME")+"   "+rs_search_TECHERE_YES.getString("LNAME"));	
								arr_search_name_tec.add(rs_search_TECHERE_YES.getInt("NO_TEC"));	
							}
						
							
						
							
					
							
					}
						
					  
					
				} catch (SQLException e1) {
					
					e1.printStackTrace();
				}
		
				
				
				
			}
		});
		
		// จบบ  ส่วนของ  คลิกเลือกสาขาวิชา แล้วชื่ออาจารย์เปลี่ยน
		
		combo_seart_code_TEH.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		combo_seart_code_TEH.setBounds(137, 325, 578, 39);
		panel_2.add(combo_seart_code_TEH);
		
		combo_search_bu_TEC = new JComboBox();
		combo_search_bu_TEC.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				

				combo_seart_code_TEH.removeAllItems();  // เมื่อมีการเปลี่ยน ให้ทำการลบ ข้อมูลที่มีใน combobox ออกก่อน
				
				//หลักการคือ  เก็บindexที่ผู้ใช้เลือก -> ค้นหารหัส สาขาวิชา -> ถ้าเจอข้อมูล -> ทำการค้นหาตามรหัส สาขาวิชา ที่เลือกเข้ามา -> เพิ่มข้อมูลไป combobox
				try {
					int index375 = combo_search_bu_TEC.getSelectedIndex();
				
					index375+=1;  // ต้อง +1 ให้มันเพราะมัน getindex ตอน select มาได้ 0 แต่ในฐานข้อมูลเป็น 1 2 3 
					String str5I = Integer.toString(index375);
					
					//ค้นหาข้อมูล index ที่ผู้ใช้ทำการเลือก 
					String sql_133 = "SELECT `Code_Bureau` FROM `bureau` WHERE `Code_Bureau`=?";
					PreparedStatement rs_bureau132 = conn.prepareStatement(sql_133);
					
					rs_bureau132.setString(1, str5I);
					ResultSet rr1ab = rs_bureau132.executeQuery();
					
					//ถ้าเจอข้อมูล
					if(rr1ab.next()){
						tana=rr1ab.getString("Code_Bureau");
						
						//ทำการค้นหา รหัสสาขาวิชา ที่ผู้ใช้เลือก
						String sql_programa = "SELECT * FROM `tb_program` WHERE `Bureau_Code_Bureau`="+rr1ab.getString("Code_Bureau");
						ResultSet rs_programx = conn.createStatement().executeQuery(sql_programa);
						
						//วนลูปทำการเพิ่มข้อมูลลงไปใน combobox
						while (rs_programx.next()) {
							
								combo_seart_code_TEH.addItem(rs_programx.getString("PROGRAM_NAME"));	
								arr_seart_code_TEH.add(rs_programx.getInt("Index_Group_Program"));	
						}				
					}

					
				} catch (SQLException e1) {
					
					e1.printStackTrace();
				}
		
			}
		});
		combo_search_bu_TEC.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		combo_search_bu_TEC.setBounds(137, 275, 578, 39);
		panel_2.add(combo_search_bu_TEC);
		
		JLabel label_2 = new JLabel("สำนักวิชา");
		label_2.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		label_2.setBounds(10, 280, 117, 29);
		panel_2.add(label_2);
		
		JLabel label_5 = new JLabel("สาขาวิชา");
		label_5.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		label_5.setBounds(10, 330, 117, 29);
		panel_2.add(label_5);
		
		
		
		//เริ่ม    ส่วนของการ เลือกชื่ออาจารย์แล้ว เอาภาพมาใส่ในช่อง
		combo_search_name_tec = new JComboBox();
		combo_search_name_tec.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				
			
				try {
					int index_name_TEC=combo_search_name_tec.getSelectedIndex();
					index_name_TEC+=1;
					
					// ตรวจสอบว่าเลือกอันไหนมา
					String sql_Z1 = "SELECT `TEACHER_CODE` FROM `bibliography_techer` WHERE `NO_TEC` =?";
					PreparedStatement rs_bureau1AZ;
					rs_bureau1AZ = conn.prepareStatement(sql_Z1);
					
					rs_bureau1AZ.setInt(1,index_name_TEC);
					ResultSet rr1AZ = rs_bureau1AZ.executeQuery();
					//ถ้าพบข้อมูล ให้ทำการอ่านขึ้นมา โดยใช้ .next()
					if(rr1AZ.next()){
						panel_pic_TEC.removeAll();
						
						Img_TEC=rr1AZ.getString("TEACHER_CODE")+".png";
						
						
					}
					
					ImageImplement panel3 = new ImageImplement(new ImageIcon("assets/imgs/Techers/"+Img_TEC).getImage());
					
					 panel_pic_TEC.add(panel3);	
					 panel_pic_TEC.doLayout();
					 
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			
				
				//จบบันทึก สำนักวิชา
				
			}
		});
		
		//จบ    ส่วนของการ เลือกชื่ออาจารย์แล้ว เอาภาพมาใส่ในช่อง
		
		combo_search_name_tec.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		combo_search_name_tec.setBounds(137, 375, 578, 39);
		panel_2.add(combo_search_name_tec);
		
		JLabel label_8 = new JLabel("ชื่ออาจารย์");
		label_8.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		label_8.setBounds(10, 385, 117, 29);
		panel_2.add(label_8);
		

		
		//ส่วนของการบันทึกข้อมูลลง Database
		JButton btn_save = new JButton("\u0E1A\u0E31\u0E19\u0E17\u0E36\u0E01\u0E02\u0E49\u0E2D\u0E21\u0E39\u0E25");
		btn_save.setIcon(new ImageIcon("C:\\Users\\clickmycom\\workspace\\Project_II\\assets\\filesave.png"));
		btn_save.setBounds(31, 496, 719, 43);
		contentPane.add(btn_save);
		btn_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					
					//เอาเลขรหัสประชาชนมาแล้วเติมตัว M ข้างหน้า
					String filesN = lblFiles.getText();
					String fileName= txt_code.getText();					
					String change_code_file="M"+fileName;
					
					//หานามสกุลของไฟล์
					String F_fileName = filesN.substring(filesN.lastIndexOf('\\')+1, filesN.length());
					String type_fileName= F_fileName.substring(F_fileName.length()-4,F_fileName.length());
					
					
					// Copy file ลงใน โฟเดอร์ img ในโปรเจค
					String desFile = null;
					try {
						desFile = new File(".").getCanonicalPath() + "\\assets\\imgs\\Students\\" + change_code_file+type_fileName;
						Files.copy(Paths.get(filesN),Paths.get(desFile),
								   StandardCopyOption.COPY_ATTRIBUTES,StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					
					
					String sql = ""
							+ " INSERT INTO bibliography_student("
							
							+ "ID_STUDENT,"
							+ "IDENTITY_CARD_NO,"
							+ "FIRSTNAME,"
							+ "FNAME,"
							+ "LNAME,"
							+ "YEAR_OF_ENTRY,"
							+ "STATUS,"
							+ "HOME_ADDRESS,"
							+ "FK_TECHER,"
							+ "FK_Code_Bureau,"
							+ "Date_in,"
							+ "FK_CODE_Program"
							+ " ) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
					
		
					int index = ddlFristName.getSelectedIndex(); //คำขึ้นต้นชื่อ
					int index1 = comboBox_Bureau.getSelectedIndex();
					int index2 = comboBox_Program.getSelectedIndex();
					int index_state=combo_status_std.getSelectedIndex();
					
					PreparedStatement pre = conn.prepareStatement(sql);
					
					
					String oo = txt_code.getText();
					pre.setString(1, "B"+oo); //สร้างรหัสนักศึกษาโดยเอา
					
					pre.setString(2, txt_code.getText());  
					  
					
//					//ทำการรับค่าที่ผู้ใช้เลือก คำนำหน้าเข้ามา และทำการค้นหาในฐานข้อูล ถ้าตรงกับเลขไหนก็เอาชื่อของคำนำหน้านั้นมาบันทึกลง
					String sq11 = "SELECT  `type_frist_name` FROM  `tb_fname_type` WHERE  `idtb_fname_type` =?";
					PreparedStatement wa = conn.prepareStatement(sq11);
					
					String ll = arrCarTypeId.get(index).toString();
					wa.setString(1, ll);
					ResultSet xxz = wa.executeQuery();
					//ถ้าพบข้อมูล ให้ทำการอ่านขึ้นมา โดยใช้ .next()
					if(xxz.next()){
						pre.setString(3, xxz.getString("type_frist_name"));
					}
//					//จบ รับประเภทคำนำหน้า
//					
					
					
					pre.setString(4, txt_Fname.getText());
					pre.setString(5, txt_Lname.getText());
					pre.setString(6, txt_date.getText());  //วันที่รับเข้า
					
					
					
					//ทำการรับค่าที่ผู้ใช้เลือก ประเภทสถานะ
					String sql_state = "SELECT  `state_name` FROM  `tb_state` WHERE  `NO` =?";
					PreparedStatement wa_state = conn.prepareStatement(sql_state);
					
					
					wa_state.setInt(1,combo_status_std.getSelectedIndex()+1);
					ResultSet xxz_state = wa_state.executeQuery();
					//ถ้าพบข้อมูล ให้ทำการอ่านขึ้นมา โดยใช้ .next()
					if(xxz_state.next()){
						pre.setString(7, xxz_state.getString("state_name"));
						
					}
//					//จบ รับประเภท ประเภทสถานะ
//					
			
					pre.setString(8,txt_Address_1_1.getText());
					
					
					
					pre.setString(9,Img_TEC.substring(0,Img_TEC.length()-4));
					
				

//					// บันทึกข้อมูล Code สำนักวิชา
					String sql_1 = "SELECT `Bureau_Name` FROM `bureau` WHERE `idBureau` =?";
					PreparedStatement rs_bureau1 = conn.prepareStatement(sql_1);
					
					String rr = arrCarTypeId.get(index1).toString();
					rs_bureau1.setString(1, rr);
					ResultSet rr1 = rs_bureau1.executeQuery();
					//ถ้าพบข้อมูล ให้ทำการอ่านขึ้นมา โดยใช้ .next()
					if(rr1.next()){
						pre.setString(10, rr1.getString("Bureau_Name"));
					}
//					//จบบันทึก สำนักวิชา
//					
//					
//					// บันทึกสาขาวิชา

					String sql_2 = "SELECT `PROGRAM_NAME` FROM `tb_program` WHERE `Index_Group_Program` =? AND `Bureau_Code_Bureau` ="+tana; //tanaคือ ค่ารหัสของ สำนักวิชา
					PreparedStatement rs_program1 = conn.prepareStatement(sql_2);
					
					String rrzc = arrCarTypeId.get(index2).toString();
					rs_program1.setString(1, rrzc);
					ResultSet rr21 = rs_program1.executeQuery();
					//ถ้าพบข้อมูล ให้ทำการอ่านขึ้นมา โดยใช้ .next()
					if(rr21.next()){
						pre.setString(11, rr21.getString("PROGRAM_NAME"));
					}
//					//จบ บันทึก สาขาวิชา
					pre.setString(12, txt_date.getText());
					
	
					//บันทึกข้อมูล
					pre.executeUpdate();
	
					int type = JOptionPane.INFORMATION_MESSAGE;
					String str = "ข้อมูลเรียบร้อยแล้ว";
					JOptionPane.showMessageDialog(null, str, "ข้อมูลเรียบร้อยแล้ว", type);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null,"บันทึกไม่สำเสร็จ");
					ex.printStackTrace();
				}
				
			}
		});
		btn_save.setFont(new Font("AngsanaUPC", Font.BOLD, 18));
		//จบ ส่วนของการบันทึกข้อมูลลง Database
		
	}
}
