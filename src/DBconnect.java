import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.SystemColor;
import java.awt.Font;
import java.awt.Color;


public class DBconnect extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JLabel label;
	private JLabel lblHostIp;
	private JLabel lblDatabase;
	private JLabel lblUsername;
	private JLabel lblPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			DBconnect dialog = new DBconnect();
			dialog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
			dialog.setVisible(true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public DBconnect() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				setLocationRelativeTo(null);
			}
		});
		setBounds(100, 100, 516, 441);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			label = new JLabel("\u0E15\u0E31\u0E49\u0E07\u0E04\u0E48\u0E32\u0E01\u0E32\u0E23\u0E40\u0E0A\u0E37\u0E48\u0E2D\u0E21\u0E15\u0E48\u0E2D\u0E40\u0E02\u0E49\u0E32\u0E01\u0E31\u0E1A\u0E10\u0E32\u0E19\u0E02\u0E49\u0E2D\u0E21\u0E39\u0E25");
			label.setFont(new Font("Angsana New", Font.BOLD | Font.ITALIC, 29));
			label.setForeground(new Color(255, 255, 255));
			label.setBounds(124, 35, 272, 35);
			contentPanel.add(label);
		}
		{
			JButton okButton = new JButton("\u0E40\u0E0A\u0E37\u0E48\u0E2D\u0E21\u0E15\u0E48\u0E2D\u0E10\u0E32\u0E19\u0E02\u0E49\u0E2D\u0E21\u0E39\u0E25");
			okButton.setIcon(new ImageIcon("C:\\Users\\clickmycom\\workspace\\Project_II\\assets\\button_ok.png"));
			okButton.setFont(new Font("Angsana New", Font.PLAIN, 22));
			okButton.setBounds(76, 287, 239, 42);
			contentPanel.add(okButton);
			okButton.setActionCommand("OK");
			getRootPane().setDefaultButton(okButton);
		}
		{
			JButton cancelButton = new JButton("\u0E22\u0E01\u0E40\u0E25\u0E34\u0E01");
			cancelButton.setIcon(new ImageIcon("C:\\Users\\clickmycom\\workspace\\Project_II\\assets\\button_cancel.png"));
			cancelButton.setFont(new Font("Angsana New", Font.PLAIN, 21));
			cancelButton.setBounds(325, 287, 121, 42);
			contentPanel.add(cancelButton);
			cancelButton.setActionCommand("Cancel");
		}
		{
			lblHostIp = new JLabel("Host / IP");
			lblHostIp.setForeground(new Color(255, 255, 255));
			lblHostIp.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblHostIp.setBounds(76, 91, 88, 14);
			contentPanel.add(lblHostIp);
		}
		{
			textField = new JTextField();
			textField.setBounds(174, 81, 272, 35);
			contentPanel.add(textField);
			textField.setColumns(10);
		}
		{
			lblPassword = new JLabel("Password");
			lblPassword.setForeground(Color.WHITE);
			lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblPassword.setBounds(76, 229, 88, 14);
			contentPanel.add(lblPassword);
		}
		{
			lblDatabase = new JLabel("Database");
			lblDatabase.setForeground(Color.WHITE);
			lblDatabase.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblDatabase.setBounds(76, 137, 88, 14);
			contentPanel.add(lblDatabase);
		}
		{
			lblUsername = new JLabel("Username");
			lblUsername.setForeground(Color.WHITE);
			lblUsername.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblUsername.setBounds(76, 183, 88, 14);
			contentPanel.add(lblUsername);
		}
		{
			textField_1 = new JTextField();
			textField_1.setColumns(10);
			textField_1.setBounds(174, 127, 272, 35);
			contentPanel.add(textField_1);
		}
		{
			textField_2 = new JTextField();
			textField_2.setColumns(10);
			textField_2.setBounds(174, 173, 272, 35);
			contentPanel.add(textField_2);
		}
		{
			textField_3 = new JTextField();
			textField_3.setColumns(10);
			textField_3.setBounds(174, 219, 272, 35);
			contentPanel.add(textField_3);
		}
		{
			JLabel lblNewLabel = new JLabel("");
			lblNewLabel.setIcon(new ImageIcon("C:\\Users\\clickmycom\\workspace\\Project_II\\assets\\login-box-backg.png"));
			lblNewLabel.setBounds(0, 0, 485, 392);
			contentPanel.add(lblNewLabel);
		}
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			buttonPane.setLayout(null);
		}
	}

}
