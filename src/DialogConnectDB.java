import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.Color;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import util.FileConnect;

import java.sql.Connection;
public class DialogConnectDB extends JFrame {

	private JPanel contentPane;
	private static JTextField txtHost;
	private static JTextField txtDatabase;
	private static JTextField txtUsername;
	private static JTextField txtPassword;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DialogConnectDB frame = new DialogConnectDB();
					frame.setVisible(true);
					txtHost.setText("");
					txtDatabase.setText("");
					txtUsername.setText("");
					txtPassword.setText("");
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DialogConnectDB() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				setLocationRelativeTo(null);
			}
		});
		
		
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setLocationRelativeTo(null);
		setUndecorated(true);
		setBounds(100, 100, 428, 351);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel v = new JLabel("Password");
		v.setForeground(Color.WHITE);
		v.setFont(new Font("Tahoma", Font.PLAIN, 18));
		v.setBounds(32, 217, 88, 14);
		contentPane.add(v);
		
		JLabel c = new JLabel("Username");
		c.setForeground(Color.WHITE);
		c.setFont(new Font("Tahoma", Font.PLAIN, 18));
		c.setBounds(32, 171, 88, 14);
		contentPane.add(c);
		
		JLabel b = new JLabel("Database");
		b.setForeground(Color.WHITE);
		b.setFont(new Font("Tahoma", Font.PLAIN, 18));
		b.setBounds(32, 125, 88, 14);
		contentPane.add(b);
		
		JLabel a = new JLabel("Host / IP");
		a.setForeground(Color.WHITE);
		a.setFont(new Font("Tahoma", Font.PLAIN, 18));
		a.setBounds(32, 79, 88, 14);
		contentPane.add(a);
		
		JLabel label_5 = new JLabel("\u0E15\u0E31\u0E49\u0E07\u0E04\u0E48\u0E32\u0E01\u0E32\u0E23\u0E40\u0E0A\u0E37\u0E48\u0E2D\u0E21\u0E15\u0E48\u0E2D\u0E40\u0E02\u0E49\u0E32\u0E01\u0E31\u0E1A\u0E10\u0E32\u0E19\u0E02\u0E49\u0E2D\u0E21\u0E39\u0E25");
		label_5.setForeground(Color.WHITE);
		label_5.setFont(new Font("Angsana New", Font.BOLD | Font.ITALIC, 29));
		label_5.setBounds(130, 23, 272, 35);
		contentPane.add(label_5);
		
		txtHost = new JTextField();
		txtHost.setColumns(10);
		txtHost.setBounds(130, 69, 272, 35);
		contentPane.add(txtHost);
		
		txtDatabase = new JTextField();
		txtDatabase.setColumns(10);
		txtDatabase.setBounds(130, 115, 272, 35);
		contentPane.add(txtDatabase);
		
		txtUsername = new JTextField();
		txtUsername.setColumns(10);
		txtUsername.setBounds(130, 161, 272, 35);
		contentPane.add(txtUsername);
		
		txtPassword = new JTextField();
		txtPassword.setColumns(10);
		txtPassword.setBounds(130, 207, 272, 35);
		contentPane.add(txtPassword);
		
		JButton ok = new JButton("\u0E40\u0E0A\u0E37\u0E48\u0E2D\u0E21\u0E15\u0E48\u0E2D\u0E10\u0E32\u0E19\u0E02\u0E49\u0E2D\u0E21\u0E39\u0E25");
		ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					if((txtHost.getText().length()==0)||(txtDatabase.getText().length()==0)||(txtUsername.getText().length()==0)||(txtPassword.getText().length()==0)){
						JOptionPane.showMessageDialog(null,"��سҷӡ�û�͹������������º����");
					}
					
					else{
						try {
							FileConnect connect = new FileConnect();
							connect.host = txtHost.getText();
							connect.database = txtDatabase.getText();
							connect.username = txtUsername.getText();
							connect.password = txtPassword.getText();
							connect.write();
							if(connect.host==null )
							JOptionPane.showMessageDialog(null, connect.host);
							
								// check ����������Ͱҹ������
								Connection c = new Connect().getConnection();
								
								if (c == null) {
									JOptionPane.showMessageDialog(null, "�������ö�ӡ���������͡Ѻ�ҹ�������� �óҷӡ�õ�Ǩ�ͺ�����١��ͧ");
								} else {			
									JOptionPane.showMessageDialog(null, "�������������º����");
									new Login().setVisible(true);
								}

						} catch (Exception ex) {
							System.out.println(ex);
							JOptionPane.showMessageDialog(null, "�������ö�ӡ������������ ex.printStackTrace()");
						}
					}
			}
		});
		ok.setFont(new Font("Angsana New", Font.PLAIN, 21));
		ok.setActionCommand("OK");
		ok.setBounds(129, 275, 142, 42);
		contentPane.add(ok);
		
		JButton cancel = new JButton("\u0E22\u0E01\u0E40\u0E25\u0E34\u0E01");
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose(); //����觻Դ
			}
		});
		cancel.setFont(new Font("Angsana New", Font.PLAIN, 21));
		cancel.setActionCommand("Cancel");
		cancel.setBounds(281, 275, 121, 42);
		contentPane.add(cancel);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon("C:\\Users\\clickmycom\\workspace\\Project_II\\assets\\xx.png"));
		label.setBounds(0, 0, 428, 352);
		contentPane.add(label);
	}
}
