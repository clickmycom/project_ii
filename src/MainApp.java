

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;

import java.awt.Font;

import javax.swing.JDesktopPane;

import java.awt.Frame;
import java.util.Locale;

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JMenuItem;

public class MainApp extends JFrame {
	private JDesktopPane desktopPane;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Locale.setDefault(Locale.ENGLISH);
			String name = "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel";
			UIManager.setLookAndFeel(name);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainApp frame = new MainApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainApp() {
		getContentPane().setBackground(SystemColor.activeCaption);
		
		desktopPane = new JDesktopPane();
		getContentPane().add(desktopPane, BorderLayout.CENTER);
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu menu = new JMenu("บันทึกรับบุคคลากร");
		menu.setIcon(new ImageIcon("C:\\Users\\clickmycom\\workspace\\Project_II\\assets\\user_add.png"));
		menuBar.add(menu);
		
		JMenuItem menuItem = new JMenuItem("อาจารย์");
		menuItem.setIcon(new ImageIcon("C:\\Users\\clickmycom\\workspace\\Project_II\\assets\\member.png"));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				Register_TEC f = new Register_TEC();
				
				
				Dimension parentSize = desktopPane.getSize();
				Dimension childSize = f.getSize();
				 
				//set center screen
				f.setLocation((parentSize.width - childSize.width)/2, (parentSize.height - childSize.height)/2);
				 
				f.setVisible(true);
				desktopPane.add(f);
				
				
				
			}
		});
		menu.add(menuItem);
		
		JMenuItem menuItem_1 = new JMenuItem("นักศึกษา");
		menuItem_1.setIcon(new ImageIcon("C:\\Users\\clickmycom\\workspace\\Project_II\\assets\\member.png"));
		menuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Register_STD f = new Register_STD();
				
				
				Dimension parentSize = desktopPane.getSize();
				Dimension childSize = f.getSize();
				 
				//set center screen
				f.setLocation((parentSize.width - childSize.width)/2, (parentSize.height - childSize.height)/2);
				 
				f.setVisible(true);
				desktopPane.add(f);
			}
		});
		menu.add(menuItem_1);
		
		JMenu menu_1 = new JMenu("ค้นหารายวิชา");
		menu_1.setIcon(new ImageIcon("C:\\Users\\clickmycom\\workspace\\Project_II\\assets\\find.png"));
		menuBar.add(menu_1);
		
		JMenuItem menuItem_2 = new JMenuItem("ค้นหารายวิชา");
		menuItem_2.setIcon(new ImageIcon("C:\\Users\\clickmycom\\workspace\\Project_II\\assets\\folder_open.png"));
		menuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Search_sub t = new Search_sub();
				
				Dimension parentSize = desktopPane.getSize();
				Dimension childSize = t.getSize();
				 
				//set center screen
				t.setLocation((parentSize.width - childSize.width)/2, (parentSize.height - childSize.height)/2);
				 
				t.setVisible(true);
				desktopPane.add(t);
			}
		});
		menu_1.add(menuItem_2);
		
		JMenu menu_2 = new JMenu("บันทึกรายวิชา");
		menu_2.setIcon(new ImageIcon("C:\\Users\\clickmycom\\workspace\\Project_II\\assets\\report_small.png"));
		menuBar.add(menu_2);
		
		JMenuItem menuItem_3 = new JMenuItem("ลงทะเบียนรายวิชา");
		menuItem_3.setIcon(new ImageIcon("C:\\Users\\clickmycom\\workspace\\Project_II\\assets\\report_small.png"));
		menuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Register_SUB z = new Register_SUB();
				
				Dimension parentSize = desktopPane.getSize();
				Dimension childSize = z.getSize();
				 
				//set center screen
				z.setLocation((parentSize.width - childSize.width)/2, (parentSize.height - childSize.height)/2);
				 
				z.setVisible(true);
				desktopPane.add(z);
				
			}
		});
		menu_2.add(menuItem_3);
		
		JMenuItem menuItem_4 = new JMenuItem("ระบุอาจารย์สอนรายวิชา");
		menuItem_4.setIcon(new ImageIcon("C:\\Users\\clickmycom\\workspace\\Project_II\\assets\\report_small.png"));
		menuItem_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				specify z = new specify();
				
				Dimension parentSize = desktopPane.getSize();
				Dimension childSize = z.getSize();
				 
				//set center screen
				z.setLocation((parentSize.width - childSize.width)/2, (parentSize.height - childSize.height)/2);
				 
				z.setVisible(true);
				desktopPane.add(z);
				
			}
		});
		menu_2.add(menuItem_4);
	}
}
