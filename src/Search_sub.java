

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.Font;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import com.mysql.jdbc.ConnectionImpl;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;

import javax.swing.JRadioButton;
import javax.swing.JTable;

import java.awt.ScrollPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;



public class Search_sub extends JInternalFrame {

	private JPanel contentPane;
	private JLabel lblFile;
	private Connection conn = new Connect().getConnection();
	
	//รับค่า เพื่อให้รู้ว่าผู้ใช้เลือกอะไร
	private ArrayList arrCarTypeId = new ArrayList(); 
	private ArrayList arrCodeId = new ArrayList(); 
	private ArrayList arrBureauId = new ArrayList(); 

	String tana="";

	
	private JTable tableResult;
	private JTextField txtCarTypeName;
	private JTextField txtCarTypePrice;
	private JTextField txtSearch;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Locale.setDefault(Locale.ENGLISH);
			String name = "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel";
			UIManager.setLookAndFeel(name);
			
			//Register_TEC frame = new Register_TEC();
			//frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Register_TEC frame = new Register_TEC();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Search_sub() {
		setClosable(true);
		setIconifiable(true);
		
		
		
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameOpened(InternalFrameEvent e) {
				
				startForm();
			}
		
			
		
		});
		
		setTitle("บันทึกรายวิชา");
		setBounds(100, 100, 776, 588);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 11, 742, 474);
		contentPane.add(tabbedPane);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("ค้นหารายวิชา", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("ค้นหา รหัสรายวิชา");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\clickmycom\\workspace\\Project_II\\assets\\find.png"));
		lblNewLabel.setBounds(327, 25, 137, 29);
		panel_1.add(lblNewLabel);
		
	
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(21, 65, 706, 370);
		panel_1.add(scrollPane);
		
		
		
		
		tableResult = new JTable();
		tableResult.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int index = tableResult.getSelectedRow();
				String carTypeName = tableResult.getValueAt(index, 1).toString();
				String carTypePrice = tableResult.getValueAt(index, 2).toString();
				
				txtCarTypeName.setText(carTypeName);
				txtCarTypePrice.setText(carTypePrice);
			}
		});
		tableResult.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null},
			},
			new String[] {
				"รหัสวิชา", "ชื่อวิชา", "หน่วยกิต","สำนักวิชา","สาขาวิชา"
			}
		));
		tableResult.getColumnModel().getColumn(0).setPreferredWidth(64);
		tableResult.getColumnModel().getColumn(1).setPreferredWidth(245);
		tableResult.getColumnModel().getColumn(3).setPreferredWidth(150);
		tableResult.getColumnModel().getColumn(4).setPreferredWidth(150);
		scrollPane.setViewportView(tableResult);
		
		JLabel label = new JLabel("ระบุรหัสรายวิชา");
		label.setBounds(142, 520, 83, 14);
		contentPane.add(label);
		
		txtSearch = new JTextField();
		txtSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				
				
try{
					
					// Step 1: เตรียมตัวแปรที่ต้องใช้งาน
					DefaultTableModel model = (DefaultTableModel) tableResult.getModel();
					Connection conn = new Connect().getConnection();
										
					// Step 3: ดึงข้อมูล และเคลียร์รายการออกจาก jTable
					ResultSet rs = conn.createStatement().executeQuery(getSQL());
					
					int totalRow = tableResult.getRowCount() - 1;
					while (totalRow > -1) {
						model.removeRow(totalRow--);
					}
					
					// Step 4: วางข้อมูลลงใน jTable
					while (rs.next()) {
						Vector row = new Vector();
						row.add(rs.getString("code_subject"));
						row.add(rs.getString("name_subject"));
						row.add(rs.getString("unit_subject"));
						row.add(rs.getString("FK_Bureau"));
						row.add(rs.getString("FK_Program"));
						
						model.addRow(row);
					}
					tableResult.setModel(model);
					
				}catch(Exception e){
					
				}
				
				
			}
		});
		txtSearch.setBounds(225, 508, 338, 38);
		contentPane.add(txtSearch);
		txtSearch.setColumns(10);
		
	
		
	}
	private void startForm() {
		try{
			// Step 1: เตรียมตัวแปรที่ต้องใช้งาน
			DefaultTableModel model = (DefaultTableModel) tableResult.getModel();
			Connection conn = new Connect().getConnection();
								
			// Step 3: ดึงข้อมูล และเคลียร์รายการออกจาก jTable
			ResultSet rs = conn.createStatement().executeQuery(getSQL());
			
			int totalRow = tableResult.getRowCount() - 1;
			while (totalRow > -1) {
				model.removeRow(totalRow--);
			}
			
			// Step 4: วางข้อมูลลงใน jTable
			while (rs.next()) {
				Vector row = new Vector();
				row.add(rs.getString("code_subject"));
				row.add(rs.getString("name_subject"));
				row.add(rs.getString("unit_subject"));
				row.add(rs.getString("FK_Bureau"));
				row.add(rs.getString("FK_Program"));
				
				model.addRow(row);
			}
			tableResult.setModel(model);
			
		}catch(Exception ex){
			
		}
		
	}
	private String getSQL() {
		String sql = ""
				+ " SELECT * FROM tb_subject WHERE "
		
				+ " code_subject LIKE('%:keyword%')"
				+ " OR name_subject LIKE('%:keyword%')"
				+ " OR unit_subject LIKE('%:keyword%')"
				+ " OR FK_Bureau LIKE('%:keyword%')"
				+ " OR FK_Program LIKE('%:keyword%')";
		sql = sql.replace(":keyword", txtSearch.getText());
		return sql;
	}
	
}
