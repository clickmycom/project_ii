
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.Image;
import java.awt.Insets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.BorderFactory;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JButton;
import javax.swing.UIManager;



















import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

import javax.swing.JTabbedPane;
import javax.swing.border.EtchedBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.table.DefaultTableModel;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.ImageIcon;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JTextArea;

import java.sql.Connection;

import javax.swing.JScrollPane;


public class View_STD extends JFrame {

	private JPanel contentPane;
	private JTextField txtCarCode;

	private ArrayList arrCarTypeId = new ArrayList();
	private ArrayList arrCarTypePrice = new ArrayList();
	private JTextField txtDay;
	private JPanel panel_1;
	 picpanel p;
	 picpanel p2;
	JPanel panel_pic;
	JPanel panel_pic_TEC;
	GridBagConstraints gbc=new GridBagConstraints();
	public  static JTextField txtDdddd;
	private JTextField txt_name;
	private JTextField txt_code;
	private JTextField txt_sum;
	private JTextField txt_saka;
	private JTextField txt_date_in;
	String get_first_name="";
	
	String code_login=Login.user_name;
	String get_f_name="";
	String get_l_name="";
	String get_code="";
	String get_sum="";
	String get_saka="";
	String get_date_in="";
	String get_address="";
	String get_state="";
	String get_teacher="";
	String code_te="";
	private JTable tableResult;
	private JTable tableResultz;
	private JTextField txtCarTypeName;
	private JTextField txtCarTypePrice;
	
	private Connection conn = new Connect().getConnection();
	private JTextField txt_state;
	private JTextField txt_teacher;
	private JTextField txt_get_code;
	private DefaultTableModel model;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Locale.setDefault(Locale.ENGLISH);
			String name = "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel";
			UIManager.setLookAndFeel(name);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					View_STD frame = new View_STD();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public View_STD() {
		
		
		startForm();
		setTitle("ข้อมูล นักศึกษา");
		setBounds(100, 100, 1001, 542);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.LEFT);
		tabbedPane.setBounds(10, 11, 965, 482);
		contentPane.add(tabbedPane);
		
		panel_1 = new JPanel();
		tabbedPane.addTab("ข้อมูลส่วนตัว", null, panel_1, null);
		panel_1.setLayout(null);
		
		
		//ส่วนของการโหลดภาพลงมาแล้วใส่รูปในกรอบ
				final JPanel panel_pic = new JPanel();
				panel_pic.setBounds(10, 11, 299, 253);
				panel_1.add(panel_pic);
				panel_pic.setLayout(new BorderLayout());
				
				//สร้าง กรอบของ Panel
				  gbc.gridx=2;
				  gbc.gridy=0;
				  gbc.gridheight=3;
				  gbc.gridwidth=3;
				  gbc.insets=new Insets(10,60,20,40);
				  Border compound,raisedbevel, loweredbevel;
				  raisedbevel = BorderFactory.createRaisedBevelBorder();
				  loweredbevel = BorderFactory.createLoweredBevelBorder();
				  compound = BorderFactory.createCompoundBorder(
							  raisedbevel, loweredbevel);
				  panel_pic.setBorder(compound);
				  panel_pic.setPreferredSize(new Dimension(100,140));
				  panel_1.add(panel_pic,gbc);  //เพิ่ม Object เข้าไปใน panel ที่ 2
				  
				  class ImageImplement extends JPanel { private Image imgxxxx; public ImageImplement(Image imgxxxx) { this.imgxxxx = imgxxxx; Dimension size = new Dimension(imgxxxx.getWidth(null), imgxxxx.getHeight(null)); setPreferredSize(size); setMinimumSize(size); setMaximumSize(size); setSize(size); setLayout(null); } public void paintComponent(Graphics g) { g.drawImage(imgxxxx, 0, 0, null); } }

				  
				  ImageImplement panel_A1 = new ImageImplement(new ImageIcon("assets/imgs/Students/"+code_login+".png").getImage());
					
				  panel_pic.add(panel_A1);	
				  panel_pic.doLayout();
				  
				  
				  
				 
				  
				
					  txtDdddd = new JTextField();
					  txtDdddd.setText(Login.user_name);
					  txtDdddd.setEditable(false);
					  txtDdddd.setColumns(10);
					  txtDdddd.setBounds(437, 11, 140, 28);
					  panel_1.add(txtDdddd);
					 
				
				  
				  JLabel label = new JLabel("รหัสประจำตัว");
				  label.setFont(new Font("Angsana New", Font.PLAIN, 19));
				  label.setBounds(319, 15, 92, 14);
				  panel_1.add(label);
				  
				  JLabel label_1 = new JLabel("ชื่อ-สกุล");
				  label_1.setFont(new Font("Angsana New", Font.PLAIN, 19));
				  label_1.setBounds(319, 65, 92, 14);
				  panel_1.add(label_1);
				  
				  txt_name = new JTextField();
				  
				  txt_name.setText(get_first_name+" "+get_f_name+"  "+get_f_name);
				  txt_name.setEditable(false);
				  txt_name.setColumns(10);
				  txt_name.setBounds(437, 61, 318, 28);
				  panel_1.add(txt_name);
				  
				  JLabel label_2 = new JLabel("เลขประจำตัวประชาชน");
				  label_2.setFont(new Font("Angsana New", Font.PLAIN, 19));
				  label_2.setBounds(319, 112, 120, 14);
				  panel_1.add(label_2);
				  
				  txt_code = new JTextField();
				  txt_code.setText(get_code);
				  txt_code.setEditable(false);
				  txt_code.setColumns(10);
				  txt_code.setBounds(437, 108, 200, 28);
				  panel_1.add(txt_code);
				  
				  JLabel label_3 = new JLabel("สำนักวิชา");
				  label_3.setFont(new Font("Angsana New", Font.PLAIN, 19));
				  label_3.setBounds(319, 153, 120, 14);
				  panel_1.add(label_3);
				  
				  JLabel label_4 = new JLabel("สาขาวิชา");
				  label_4.setFont(new Font("Angsana New", Font.PLAIN, 19));
				  label_4.setBounds(319, 188, 120, 14);
				  panel_1.add(label_4);
				  
				  JLabel label_5 = new JLabel("ปีการศึกษาที่เข้า");
				  label_5.setFont(new Font("Angsana New", Font.PLAIN, 19));
				  label_5.setBounds(319, 222, 120, 14);
				  panel_1.add(label_5);
				  
				  txt_sum = new JTextField();
				  txt_sum.setText(get_sum);
				  txt_sum.setEditable(false);
				  txt_sum.setColumns(10);
				  txt_sum.setBounds(437, 153, 200, 28);
				  panel_1.add(txt_sum);
				  
				  txt_saka = new JTextField();
				  txt_saka.setText(get_saka);
				  txt_saka.setEditable(false);
				  txt_saka.setColumns(10);
				  txt_saka.setBounds(437, 188, 200, 28);
				  panel_1.add(txt_saka);
				  
				  txt_date_in = new JTextField();
				  txt_date_in.setText(get_date_in);
				  txt_date_in.setEditable(false);
				  txt_date_in.setColumns(10);
				  txt_date_in.setBounds(437, 222, 200, 28);
				  panel_1.add(txt_date_in);
				  
				  JLabel label_6 = new JLabel("ที่อยู่");
				  label_6.setFont(new Font("Angsana New", Font.PLAIN, 19));
				  label_6.setBounds(319, 274, 120, 23);
				  panel_1.add(label_6);
				  
				  JTextArea txt_address = new JTextArea();
				  txt_address.setText(get_address);
				  txt_address.setEditable(false);

				  txt_address.setBounds(437, 276, 318, 79);
				  panel_1.add(txt_address);
				  
				  JLabel label_7 = new JLabel("สถานะ");
				  label_7.setFont(new Font("Angsana New", Font.PLAIN, 19));
				  label_7.setBounds(319, 369, 120, 14);
				  panel_1.add(label_7);
				  
				  JLabel label_8 = new JLabel("อาจารย์ที่ปรึกษา");
				  label_8.setFont(new Font("Angsana New", Font.PLAIN, 19));
				  label_8.setBounds(319, 411, 120, 14);
				  panel_1.add(label_8);
				  
				  txt_state = new JTextField();
				  txt_state.setText(get_state);
				  txt_state.setEditable(false);
				  txt_state.setColumns(10);
				  txt_state.setBounds(437, 369, 200, 28);
				  panel_1.add(txt_state);
				  
				  txt_teacher = new JTextField();
				  txt_teacher.setText(get_teacher);
				  txt_teacher.setEditable(false);
				  txt_teacher.setColumns(10);
				  txt_teacher.setBounds(437, 411, 200, 28);
				  panel_1.add(txt_teacher);
				  
				  
				 // จบการสร้างกรอบ Panel
		  
				 
				 
				  
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("ตารางเรียน", null, panel_2, null);
		panel_2.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 865, 370);
		panel_2.add(scrollPane);
		
		tableResult = new JTable();
		tableResult.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int index = tableResult.getSelectedRow();
			
			}
		});
		tableResult.setModel(new DefaultTableModel(
			new Object[][] {
				
						{ "จันทร์"},
						{ "อังคาร"},
						{ "พุธ"},
						{ "พฤหัสบดี"},
						{ "ศุกร์"}
			},
			new String[] {
					"วัน","8:00-9:00", "9:00-10:00", "10:00-11:00","11:00-12:00","12:00-13:00,","12:00-13:00","13:00-14:00","14:00-15:00","15:00-16:00","16:00-17:00","17:00-18:00"
					
			}
		));
		tableResult.getColumnModel().getColumn(0).setPreferredWidth(52);
		scrollPane.setViewportView(tableResult);
		
		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("ลงทะเบียน", null, panel_3, null);
		panel_3.setLayout(null);
		
		txt_get_code = new JTextField();


		txt_get_code.setColumns(10);
		txt_get_code.setBounds(316, 42, 200, 28);
		panel_3.add(txt_get_code);
		
		
		
		
		
		
		
		
		JScrollPane scrollPane_reg = new JScrollPane();
		scrollPane_reg.setBounds(10, 186, 865, 244);
		panel_3.add(scrollPane_reg);
		
		tableResultz = new JTable();
		tableResultz.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int index = tableResultz.getSelectedRow();
			
			}
		});
		tableResultz.setModel(new DefaultTableModel(
			new Object[][] {
				
					{null, null, null,null},
			},
			new String[] {
					"ลำดับที่","รหัสวิชา", "ชื่อวิชา", "หน่วยกิต","เงื่อนไขรายวิชา"
					
			}
		));
		tableResultz.getColumnModel().getColumn(0).setPreferredWidth(42);
		scrollPane_reg.setViewportView(tableResultz);
		
		
		
		
		JLabel label_9 = new JLabel("รหัสรายวิชา");
		label_9.setFont(new Font("Angsana New", Font.PLAIN, 19));
		label_9.setBounds(57, 46, 92, 14);
		panel_3.add(label_9);
		
		JButton btn_submit_code = new JButton("ลงทะเบียน");
		btn_submit_code.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try{
					String sql_re = ""
							+ " INSERT INTO tb_register_subject("
							
							+ "FK_ID_STUDENT,"
							+ "FK_code_subject"
							
							+ " ) VALUES(?,?)";
					PreparedStatement pre = conn.prepareStatement(sql_re);
					
					pre.setString(1, Login.user_name);  
					pre.setString(2, txt_get_code.getText());  
					
					pre.executeUpdate();
					loadData();
					
					int type = JOptionPane.INFORMATION_MESSAGE;
					String str = "ข้อมูลเรียบร้อยแล้ว";
					JOptionPane.showMessageDialog(null, str, "ข้อมูลเรียบร้อยแล้ว", type);
					
					
					
				}catch(Exception e){
					JOptionPane.showMessageDialog(null,"บันทึกไม่สำเสร็จ");
				}
				
				
				
			}
		});
		btn_submit_code.setBounds(542, 41, 133, 30);
		panel_3.add(btn_submit_code);
		
		JButton btn_delete = new JButton("ลบรายวิชา");
		btn_delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					int index = tableResultz.getSelectedRow();
					String id = tableResultz.getValueAt(index, 0).toString();
					String sql5 = "DELETE FROM tb_register_subject WHERE NO=" + id;
					
					conn.createStatement().executeUpdate(sql5);
					loadData();
					JOptionPane.showMessageDialog(null,id);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
		btn_delete.setBounds(685, 42, 112, 28);
		panel_3.add(btn_delete);
		
		
		
	}

	private void startForm() {
		// TODO Auto-generated method stub
		
		
		try {
			String sql = "SELECT * FROM `bibliography_student` WHERE `ID_STUDENT` ="+"'"+code_login+"'";
			ResultSet rs = conn.createStatement().executeQuery(sql);
			
		
			while (rs.next()) {
				get_first_name=rs.getString("FIRSTNAME");
				get_f_name=rs.getString("FNAME");
				get_l_name=rs.getString("LNAME");
				get_code=rs.getString("IDENTITY_CARD_NO");
				get_sum=rs.getString("FK_Code_Bureau");
				get_saka=rs.getString("FK_CODE_Program");
				get_address=rs.getString("HOME_ADDRESS");
				get_date_in=rs.getString("Date_in");
				get_state=rs.getString("STATUS");
				code_te=rs.getString("FK_TECHER");
			}	
			
			String sql_TE = "SELECT * FROM `bibliography_techer` WHERE `TEACHER_CODE` ="+"'"+code_te+"'";
			ResultSet rs_E = conn.createStatement().executeQuery(sql_TE);
			
			while(rs_E.next()){
				get_teacher=rs_E.getString("FIRSTNAME")+"  "+rs_E.getString("FNAME")+"  "+rs_E.getString("LNAME");
			}
				
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
	}
	
	private void loadData() {
		// Step 1: เตรียมตัวแปรที่ต้องใช้งาน
		DefaultTableModel model = (DefaultTableModel) tableResultz.getModel();
		Connection conn = new Connect().getConnection();
		
		try {
			// clear data
			int totalRow = tableResultz.getRowCount() - 1;
			while (totalRow > -1) {
				model.removeRow(totalRow--);
			}
			
			// add data
			String sql = "SELECT * FROM tb_register_subject INNER JOIN tb_subject ON tb_register_subject.FK_code_subject=tb_subject.code_subject WHERE FK_ID_STUDENT="+"'"+code_login+"'";
			ResultSet rs = conn.createStatement().executeQuery(sql);
			
			
			while (rs.next()) {
				Vector row = new Vector();
				row.add(rs.getString("NO"));
				row.add(rs.getString("FK_code_subject"));
				row.add(rs.getString("name_subject"));
				row.add(rs.getString("unit_subject"));
				row.add(rs.getString("condition_subject"));
				
				model.addRow(row);
			}
			tableResultz.setModel(model);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
