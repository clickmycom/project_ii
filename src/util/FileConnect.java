package util;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.commons.io.FileUtils;

public class FileConnect {
	
	public String host;
	public String username;
	public String password;
	public String database;
	
	public void write() {
		try {
			File file = new File("connect.config");
			
			String data = String.format("host=%s\r\nusername=%s\r\npassword=%s\r\ndatabase=%s", host, username, password, database);
			FileUtils.writeStringToFile(file, data);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public Connection read() {
		try {
			File file = new File("connect.config");
			
			if (file.exists()) {
				String data = FileUtils.readFileToString(file);
				String[] row = data.split("\r\n");
				
				this.host = row[0].replace("host=", "");
				this.username = row[1].replace("username=", "");
				this.password = row[2].replace("password=", "");
				this.database = row[3].replace("database=", "");
				
				String url = "jdbc:mysql://" + host + "/" + database + "?characaterEncoding=UTF-8";
				Class.forName("com.mysql.jdbc.Driver");
				return DriverManager.getConnection(url, username, password);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
}
