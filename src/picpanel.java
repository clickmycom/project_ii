 import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.ImageIcon.*;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.sql.*;

class picpanel extends JPanel
{
 Image imgp;
 picpanel(Image img)
 {
  this.imgp = img;
  repaint();
 }
 
// Fires whenever this panel is resized
 public void paintComponent(Graphics g) 
 {
  super.paintComponent(g);
  g.drawImage(imgp, 0, 0, getWidth(), getHeight(),this);
 }
}

