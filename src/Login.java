

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JButton;







import javax.swing.UIManager;











import java.awt.Font;
import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Locale;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class Login extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsername;
	private JTextField txtPassword;
	private JLabel lblNewLabel_1;
	private JLabel label;
	private JLabel label_1;
	public static boolean login = false;
	

	
	public static int user_id = 0;
	public static String user_name = "";
	public static String user_username = "";
	public static String user_level = "";
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		try {
			Locale.setDefault(Locale.ENGLISH);
			String name = "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel";
			UIManager.setLookAndFeel(name);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				setLocationRelativeTo(null);
				
				
				
			}
		});
		
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1007, 512);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtUsername = new JTextField();
		txtUsername.setBounds(625, 110, 331, 32);
		contentPane.add(txtUsername);
		txtUsername.setColumns(10);
		
		txtPassword = new JTextField();
		txtPassword.setColumns(10);
		txtPassword.setBounds(625, 169, 331, 32);
		contentPane.add(txtPassword);
		
		label_1 = new JLabel("\u0E01\u0E23\u0E38\u0E13\u0E32\u0E1B\u0E49\u0E2D\u0E19\u0E23\u0E2B\u0E31\u0E2A\u0E1B\u0E23\u0E30\u0E08\u0E33\u0E15\u0E31\u0E27\u0E41\u0E25\u0E30\u0E23\u0E2B\u0E31\u0E2A\u0E1C\u0E48\u0E32\u0E19");
		label_1.setForeground(Color.RED);
		label_1.setFont(new Font("Tahoma", Font.BOLD, 22));
		label_1.setBounds(461, 11, 372, 27);
		contentPane.add(label_1);
		
		lblNewLabel_1 = new JLabel("\u0E23\u0E2B\u0E31\u0E2A\u0E1B\u0E23\u0E30\u0E08\u0E33\u0E15\u0E31\u0E27");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(527, 119, 76, 14);
		contentPane.add(lblNewLabel_1);
		
		label = new JLabel("\u0E23\u0E2B\u0E31\u0E2A\u0E1C\u0E48\u0E32\u0E19");
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setBounds(527, 178, 76, 14);
		contentPane.add(label);
		
		JButton btnNewButton = new JButton("\u0E40\u0E02\u0E49\u0E32\u0E2A\u0E39\u0E48\u0E23\u0E30\u0E1A\u0E1A");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					
					// ตัวเชื่อมต่อ
					Connection conn = new Connect().getConnection();
					
					// SQL
					String sql = ""
							+ " SELECT * FROM user "
							+ " WHERE User_name=? AND Pass=?";
					PreparedStatement pre = conn.prepareStatement(sql);
					pre.setString(1, txtUsername.getText());
					pre.setString(2, txtPassword.getText());
					
					// ดึงข้อมูล
					ResultSet rs = pre.executeQuery();
					
					// ตรวจสอบว่า username ผ่านหรือไม่
					if (rs.next()) {
						
						
						Login.user_level = rs.getString("user_level");
						Login.user_name = rs.getString("User_name");
						
//						System.out.print(rs.getString("user_level"));
						
						if (Login.user_level.equals("manager")) {
							MainApp d = new MainApp();
							d.setVisible(true);
						}else if(Login.user_level.equals("employee_TECH")){
							
							View_TEC v = new View_TEC();
							v.setVisible(true);
							
						}else if(Login.user_level.equals("employee_STD")){
							View_STD k = new View_STD();
							k.setVisible(true);
						}
						
					
						dispose();
					} else {
						int type = JOptionPane.ERROR_MESSAGE;
						String msg = "ไม่มีชื่อผู้ใช้ในระบบ";
						String title = "ตรวจสอบการเข้าระบบ";
						
						JOptionPane.showMessageDialog(null, msg, title, type);
					}
					
				}catch(Exception e){
					
				}
				
				
				
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 21));
		btnNewButton.setBounds(625, 231, 331, 57);
		contentPane.add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\clickmycom\\workspace\\Project_II\\assets\\login.png"));
		lblNewLabel.setBounds(0, 0, 1000, 474);
		contentPane.add(lblNewLabel);
	}
}
