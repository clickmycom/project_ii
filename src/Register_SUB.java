


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.Font;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import com.mysql.jdbc.ConnectionImpl;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;

import javax.swing.JRadioButton;
import javax.swing.JTable;

import java.awt.ScrollPane;



public class Register_SUB extends JInternalFrame {

	private JPanel contentPane;
	private JTextField txt_Fname;
	private JLabel lblFile;
	private Connection conn = new Connect().getConnection();
	
	private JComboBox ddlFristName;
	private JComboBox comboBox_Bureau;
	private JComboBox comboBox_Program;
	
	//รับค่า เพื่อให้รู้ว่าผู้ใช้เลือกอะไร
	private ArrayList arrCarTypeId = new ArrayList(); 
	private ArrayList arrCodeId = new ArrayList(); 
	private ArrayList arrBureauId = new ArrayList(); 
	
	private JTextField txt_Lname;
	private JTextField txt_code;
	private JTextField txt_Old;
	private JTextComponent txt_Address_1;
	String tana="";
	
	File file;
	Image img;
	 picpanel p;
	JPanel panel_pic;
	GridBagConstraints gbc=new GridBagConstraints();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Locale.setDefault(Locale.ENGLISH);
			String name = "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel";
			UIManager.setLookAndFeel(name);
		
			//frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Register_SUB frame = new Register_SUB();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Register_SUB() {
		setClosable(true);
		setIconifiable(true);
		
		
		
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameOpened(InternalFrameEvent e) {
				
				startForm();
			}
		
			
			private void startForm() {
				try {
					
					
					//สำนักวิชา
					String sql_rs_bureau = "SELECT * FROM bureau";
					ResultSet rs_bureau = conn.createStatement().executeQuery(sql_rs_bureau);

					while (rs_bureau.next()) {
						
						comboBox_Bureau.addItem(rs_bureau.getString("Bureau_Name"));	
						arrBureauId.add(rs_bureau.getInt("Code_Bureau"));
					}

					
					// กำหนดค่าเช่าเริ่มต้น คิดจากค่าใน dropdownList
					
					int index1 = comboBox_Bureau.getSelectedIndex();
					int index2 = comboBox_Program.getSelectedIndex();
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		setTitle("บันทึกรายวิชา");
		setBounds(100, 100, 776, 588);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 11, 742, 474);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("ข้อมูลรายวิชา", null, panel, null);
		panel.setLayout(null);
		
		JLabel label_1 = new JLabel("จำนวนหน่วยกิต");
		label_1.setBounds(22, 321, 117, 14);
		panel.add(label_1);
		label_1.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		
	
		
		JLabel Lname = new JLabel("รหัสวิชา");
		Lname.setBounds(22, 158, 46, 14);
		panel.add(Lname);
		Lname.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		
		final JTextField txt_subject = new JTextField();
		txt_subject.setBounds(149, 87, 578, 45);
		panel.add(txt_subject);
		txt_subject.setFont(new Font("Angsana New", Font.PLAIN, 16));
		txt_subject.setColumns(10);
		
		JLabel Fname = new JLabel("ชื่อวิชา");
		Fname.setBounds(22, 102, 46, 14);
		panel.add(Fname);
		Fname.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		
		comboBox_Bureau = new JComboBox();
		comboBox_Bureau.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				comboBox_Program.removeAllItems();  // เมื่อมีการเปลี่ยน ให้ทำการลบ ข้อมูลที่มีใน combobox ออกก่อน
				
				//หลักการคือ  เก็บindexที่ผู้ใช้เลือก -> ค้นหารหัส สาขาวิชา -> ถ้าเจอข้อมูล -> ทำการค้นหาตามรหัส สาขาวิชา ที่เลือกเข้ามา -> เพิ่มข้อมูลไป combobox
				try {
					int index31 = comboBox_Bureau.getSelectedIndex();
				
					index31+=1;  // ต้อง +1 ให้มันเพราะมัน getindex ตอน select มาได้ 0 แต่ในฐานข้อมูลเป็น 1 2 3 
					String strI = Integer.toString(index31);
					
					//ค้นหาข้อมูล index ที่ผู้ใช้ทำการเลือก 
					String sql_1 = "SELECT `Code_Bureau` FROM `bureau` WHERE `Code_Bureau`=?";
					PreparedStatement rs_bureau1 = conn.prepareStatement(sql_1);
					
					rs_bureau1.setString(1, strI);
					ResultSet rr1 = rs_bureau1.executeQuery();
					
					//ถ้าเจอข้อมูล
					if(rr1.next()){
						tana=rr1.getString("Code_Bureau");
						
						//ทำการค้นหา รหัสสาขาวิชา ที่ผู้ใช้เลือก
						String sql_program = "SELECT * FROM `tb_program` WHERE `Bureau_Code_Bureau`="+rr1.getString("Code_Bureau");
						ResultSet rs_program = conn.createStatement().executeQuery(sql_program);
						
						//วนลูปทำการเพิ่มข้อมูลลงไปใน combobox
						while (rs_program.next()) {
							
								comboBox_Program.addItem(rs_program.getString("PROGRAM_NAME"));	
								arrCodeId.add(rs_program.getInt("Index_Group_Program"));	
						}				
					}

					
				} catch (SQLException e1) {
					
					e1.printStackTrace();
				}
				
				
			}
		});
		comboBox_Bureau.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		comboBox_Bureau.setBounds(149, 199, 578, 39);
		panel.add(comboBox_Bureau);
		
		comboBox_Program = new JComboBox();
		comboBox_Program.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		comboBox_Program.setBounds(149, 256, 578, 39);
		panel.add(comboBox_Program);
		
		JLabel label_6 = new JLabel("\u0E2A\u0E33\u0E19\u0E31\u0E01\u0E27\u0E34\u0E0A\u0E32");
		label_6.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		label_6.setBounds(22, 204, 117, 29);
		panel.add(label_6);
		
		JLabel label_7 = new JLabel("\u0E2A\u0E32\u0E02\u0E32\u0E27\u0E34\u0E0A\u0E32");
		label_7.setFont(new Font("AngsanaUPC", Font.PLAIN, 18));
		label_7.setBounds(22, 261, 117, 29);
		panel.add(label_7);
		
		final JRadioButton radioButton_2 = new JRadioButton("2 หน่วย");
		radioButton_2.setBounds(226, 319, 73, 23);
		panel.add(radioButton_2);
		
		final JRadioButton radioButton = new JRadioButton("1 หน่วย");
		radioButton.setBounds(149, 319, 75, 23);
		panel.add(radioButton);
		
		final JRadioButton radioButton_1 = new JRadioButton("3 หน่วย");
		radioButton_1.setBounds(301, 319, 79, 23);
		panel.add(radioButton_1);
		
		final JRadioButton radioButton_3 = new JRadioButton("4 หน่วย");
		radioButton_3.setBounds(382, 319, 69, 23);
		panel.add(radioButton_3);
		
		final JRadioButton radioButton_4 = new JRadioButton("5 หน่วย");
		radioButton_4.setBounds(469, 319, 69, 23);
		panel.add(radioButton_4);
		
		final JRadioButton radioButton_5 = new JRadioButton("6 หน่วย");
		radioButton_5.setBounds(552, 319, 64, 23);
		panel.add(radioButton_5);
		
		//จัด group
		ButtonGroup group = new ButtonGroup();
		group.add(radioButton);
		group.add(radioButton_2);
		group.add(radioButton_1);
		group.add(radioButton_3);
		group.add(radioButton_4);
		group.add(radioButton_5);
		
	
		final JTextField txt_condition = new JTextField();
		txt_condition.setFont(new Font("Angsana New", Font.PLAIN, 16));
		txt_condition.setColumns(10);
		txt_condition.setBounds(149, 368, 578, 45);
		panel.add(txt_condition);
		
		final JTextField code_sub = new JTextField();
		code_sub.setFont(new Font("Angsana New", Font.PLAIN, 16));
		code_sub.setColumns(10);
		code_sub.setBounds(149, 143, 578, 45);
		panel.add(code_sub);
		
		JLabel label = new JLabel("เงื่อนไขรายวิชา");
		label.setBounds(22, 385, 117, 14);
		panel.add(label);
		


		//ส่วนของการบันทึกข้อมูลลง Database
		JButton btn_save = new JButton("\u0E1A\u0E31\u0E19\u0E17\u0E36\u0E01\u0E02\u0E49\u0E2D\u0E21\u0E39\u0E25");
		btn_save.setIcon(new ImageIcon("C:\\Users\\clickmycom\\workspace\\Project_II\\assets\\filesave.png"));
		btn_save.setBounds(10, 496, 740, 43);
		contentPane.add(btn_save);
		btn_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//เริ่ม  ตรวจสอบว่าเลือก radio อันไหน
				int unit_subject = 0;
				
				if (radioButton.isSelected()) {
					unit_subject=1;
				}else if(radioButton_2.isSelected()){
					unit_subject=2;
				}else if(radioButton_1.isSelected()){
					unit_subject=3;
				}else if(radioButton_3.isSelected()){
					unit_subject=4;
				}else if(radioButton_4.isSelected()){
					unit_subject=5;
				}else if(radioButton_5.isSelected()){
					unit_subject=6;
				}
				//จบ  ตรวจสอบว่าเลือก radio อันไหน
				
				
				try {
					
					String sql = ""
							+ " INSERT INTO tb_subject("
							
							+ "code_subject,"
							+ "name_subject,"
							+ "unit_subject,"
							+ "condition_subject,"
							+ "FK_Program,"
							+ "FK_Bureau"
							+ " ) VALUES(?,?,?,?,?,?)";
					
		
				
					int index1 = comboBox_Bureau.getSelectedIndex();
					int index2 = comboBox_Program.getSelectedIndex();

					
					PreparedStatement pre = conn.prepareStatement(sql);
					

					pre.setString(1, code_sub.getText());
					pre.setString(2, txt_subject.getText());
					
					pre.setInt(3, unit_subject);
					pre.setString(4, txt_condition.getText());
					

					// บันทึกข้อมูล Code สำนักวิชา
					String sql_1 = "SELECT `Bureau_Name` FROM `bureau` WHERE `idBureau` =?";
					PreparedStatement rs_bureau1 = conn.prepareStatement(sql_1);
					
					String rr = arrBureauId.get(index1).toString();
					rs_bureau1.setString(1, rr);
					ResultSet rr1 = rs_bureau1.executeQuery();
					//ถ้าพบข้อมูล ให้ทำการอ่านขึ้นมา โดยใช้ .next()
					if(rr1.next()){
						pre.setString(5, rr1.getString("Bureau_Name"));
					}
					//จบบันทึก สำนักวิชา
					
					
					
					// บันทึกสาขาวิชา

					String sql_2 = "SELECT `PROGRAM_NAME` FROM `tb_program` WHERE `Index_Group_Program` =? AND `Bureau_Code_Bureau` ="+tana; //tanaคือ ค่ารหัสของ สำนักวิชา
					PreparedStatement rs_program1 = conn.prepareStatement(sql_2);
					
					String rrzc = arrCodeId.get(index2).toString();
					rs_program1.setString(1, rrzc);
					ResultSet rr21 = rs_program1.executeQuery();
					//ถ้าพบข้อมูล ให้ทำการอ่านขึ้นมา โดยใช้ .next()
					if(rr21.next()){
						pre.setString(6, rr21.getString("PROGRAM_NAME"));
					}
					//จบ บันทึก สาขาวิชา
					
	
					//บันทึกข้อมูล
					pre.executeUpdate();
	
					int type = JOptionPane.INFORMATION_MESSAGE;
					String str = "ข้อมูลเรียบร้อยแล้ว";
					JOptionPane.showMessageDialog(null, str, "ข้อมูลเรียบร้อยแล้ว", type);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null,"ไม่เข้า");
					ex.printStackTrace();
				}
				
			}
		});
		btn_save.setFont(new Font("AngsanaUPC", Font.BOLD, 18));
		//จบ ส่วนของการบันทึกข้อมูลลง Database
		
	}
}
