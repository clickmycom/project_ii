import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JDesktopPane;
import java.awt.Frame;
import javax.swing.JMenu;
import java.awt.Font;


public class Admin extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Admin frame = new Admin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Admin() {
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 885, 581);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu menu = new JMenu("\u0E25\u0E07\u0E17\u0E30\u0E40\u0E1A\u0E35\u0E22\u0E19\u0E1A\u0E38\u0E04\u0E25\u0E32\u0E01\u0E23");
		menu.setFont(new Font("Angsana New", Font.BOLD, 18));
		menuBar.add(menu);
		
		JMenu menu_1 = new JMenu("\u0E19\u0E31\u0E01\u0E28\u0E36\u0E01\u0E29\u0E32");
		menu_1.setFont(new Font("Angsana New", Font.PLAIN, 18));
		menu.add(menu_1);
		
		JMenu menu_2 = new JMenu("\u0E2D\u0E32\u0E08\u0E32\u0E23\u0E22\u0E4C");
		menu_2.setFont(new Font("Angsana New", Font.PLAIN, 18));
		menu.add(menu_2);
		
		JMenu menu_3 = new JMenu("\u0E23\u0E32\u0E22\u0E07\u0E32\u0E19");
		menu_3.setFont(new Font("Angsana New", Font.BOLD, 18));
		menuBar.add(menu_3);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		jBG desktopPane = new jBG();
		contentPane.add(desktopPane, BorderLayout.CENTER);
	}

}
